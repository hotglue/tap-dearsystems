"""hotglue tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_dearsystems.streams import (
    AdvancePurchaseStream,
    ProductAvalibilityStream,
    ProductStream,
    ProductProductionBOM,
    PurchaseStream,
    SaleDetailsStream,
    SaleListStream,
    SupplierStream,
    LocationStream,
    MeStream,
    ProductionOrderDetailStream,
    ProductionOrderListStream,
    TaxRulesStream,
    UnitListStream,
    StockTransferListStream,
    StockTransferStream
)

STREAM_TYPES = [
    ProductStream,
    ProductProductionBOM,
    SaleListStream,
    SaleDetailsStream,
    ProductAvalibilityStream,
    SupplierStream,
    PurchaseStream,
    AdvancePurchaseStream,
    LocationStream,
    MeStream,
    ProductionOrderDetailStream,
    ProductionOrderListStream,
    TaxRulesStream,
    UnitListStream,
    StockTransferListStream,
    StockTransferStream
]


class TapDearsystems(Tap):
    """Dearsystems tap class."""

    name = "tap-dearsystems"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property("api_account_id", th.StringType, required=True),
        th.Property("api_key", th.StringType, required=True),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapDearsystems.cli()
