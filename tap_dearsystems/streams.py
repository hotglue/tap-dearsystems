"""Stream type classes for tap-dearsystems."""

from typing import Optional
from singer_sdk import typing as th
from tap_dearsystems.client import hotglueStream, ChildStream


class ProductStream(hotglueStream):
    name = "product"
    path = "/product"
    primary_keys = ["ID"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.Products[*]"
    additional_params = {
        "IncludeSuppliers": "true",
        "IncludeCustomPrices": "true",
        "IncludeDeprecated": "true",
        "IncludeBOM": "true",
    }

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("SKU", th.StringType),
        th.Property("modifiedDate", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Category", th.StringType),
        th.Property("Brand", th.StringType),
        th.Property("Type", th.StringType),
        th.Property("CostingMethod", th.StringType),
        th.Property("DropShipMode", th.StringType),
        th.Property("DefaultLocation", th.StringType),
        th.Property("Length", th.NumberType),
        th.Property("Width", th.NumberType),
        th.Property("Height", th.NumberType),
        th.Property("Weight", th.NumberType),
        th.Property("UOM", th.StringType),
        th.Property("WeightUnits", th.StringType),
        th.Property("DimensionsUnits", th.StringType),
        th.Property("Barcode", th.StringType),
        th.Property("MinimumBeforeReorder", th.NumberType),
        th.Property("ReorderQuantity", th.NumberType),
        th.Property("PriceTier1", th.NumberType),
        th.Property("PriceTier2", th.NumberType),
        th.Property("PriceTier3", th.NumberType),
        th.Property("PriceTier4", th.NumberType),
        th.Property("PriceTier5", th.NumberType),
        th.Property("PriceTier6", th.NumberType),
        th.Property("PriceTier7", th.NumberType),
        th.Property("PriceTier8", th.NumberType),
        th.Property("PriceTier9", th.NumberType),
        th.Property("PriceTier10", th.NumberType),
        th.Property("AverageCost", th.NumberType),
        th.Property("ShortDescription", th.StringType),
        th.Property("InternalNote", th.StringType),
        th.Property("AdditionalAttribute1", th.StringType),
        th.Property("AdditionalAttribute2", th.StringType),
        th.Property("AdditionalAttribute3", th.StringType),
        th.Property("AdditionalAttribute4", th.StringType),
        th.Property("AdditionalAttribute5", th.StringType),
        th.Property("AdditionalAttribute6", th.StringType),
        th.Property("AdditionalAttribute7", th.StringType),
        th.Property("AdditionalAttribute8", th.StringType),
        th.Property("AdditionalAttribute9", th.StringType),
        th.Property("AdditionalAttribute10", th.StringType),
        th.Property("AttributeSet", th.StringType),
        th.Property("DiscountRule", th.StringType),
        th.Property("Tags", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("StockLocator", th.StringType),
        th.Property("COGSAccount", th.StringType),
        th.Property("RevenueAccount	", th.StringType),
        th.Property("ExpenseAccount", th.StringType),
        th.Property("InventoryAccount", th.StringType),
        th.Property("PurchaseTaxRule", th.StringType),
        th.Property("SaleTaxRule", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
        th.Property("Sellable", th.BooleanType),
        th.Property("PickZones", th.StringType),
        th.Property("BillOfMaterial", th.BooleanType),
        th.Property("AutoAssembly", th.BooleanType),
        th.Property("AutoDisassembly", th.BooleanType),
        th.Property("QuantityToProduce", th.NumberType),
        th.Property("AssemblyInstructionURL", th.StringType),
        th.Property("AssemblyCostEstimationMethod", th.StringType),
        th.Property("BOMType", th.StringType),
        th.Property(
            "Suppliers",
            th.ArrayType(
                th.ObjectType(
                    th.Property("SupplierID", th.StringType),
                    th.Property("SupplierName", th.StringType),
                    th.Property("SupplierInventoryCode", th.StringType),
                    th.Property("SupplierProductName", th.StringType),
                    th.Property("Cost", th.NumberType),
                    th.Property("FixedCost", th.NumberType),
                    th.Property("Currency", th.StringType),
                    th.Property("DropShip", th.BooleanType),
                    th.Property("URL", th.StringType),
                )
            ),
        ),
        th.Property(
            "ReorderLevels",
            th.ArrayType(
                th.ObjectType(
                    th.Property("LocationID", th.StringType),
                    th.Property("LocationName", th.StringType),
                    th.Property("MinimumBeforeReorder", th.NumberType),
                    th.Property("ReorderQuantity", th.NumberType),
                    th.Property("StockLocator", th.StringType),
                    th.Property("PickZones", th.StringType),
                )
            ),
        ),
        th.Property(
            "BillOfMaterialsProducts",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ComponentProductID", th.StringType),
                    th.Property("ProductCode", th.StringType),
                    th.Property("Name", th.StringType),
                    th.Property("Quantity", th.NumberType),
                    th.Property("WastagePercent", th.NumberType),
                    th.Property("WastageQuantity", th.NumberType),
                    th.Property("CostPercentage", th.NumberType),
                )
            ),
        ),
        th.Property(
            "BillOfMaterialsServices",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ComponentProductID", th.StringType),
                    th.Property("ExpenseAccount", th.StringType),
                    th.Property("Name", th.StringType),
                    th.Property("Quantity", th.NumberType),
                    th.Property("PriceTier", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "Movements",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TaskID", th.StringType),
                    th.Property("Type", th.StringType),
                    th.Property("Date", th.StringType),
                    th.Property("Number", th.StringType),
                    th.Property("Status", th.IntegerType),
                    th.Property("Quantity", th.NumberType),
                    th.Property("Amount", th.NumberType),
                    th.Property("Location", th.NumberType),
                    th.Property("BatchSN", th.NumberType),
                    th.Property("ExpiryDate", th.StringType),
                    th.Property("FromTo", th.StringType),
                )
            ),
        ),
        th.Property(
            "Attachments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ID", th.StringType),
                    th.Property("ContentType", th.StringType),
                    th.Property("IsDefault", th.BooleanType),
                    th.Property("FileName", th.StringType),
                    th.Property("DownloadUrl", th.IntegerType),
                )
            ),
        ),
        th.Property(
            "CustomPrices",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ProductID", th.StringType),
                    th.Property("CustomerID", th.StringType),
                    th.Property("CustomerName", th.StringType),
                    th.Property("ProductSKU", th.StringType),
                    th.Property("ProductName", th.StringType),
                    th.Property("Price", th.NumberType),
                )
            ),
        ),
        th.Property("BOMType", th.StringType),
        th.Property("WarrantyName", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        if record["BOMType"] != "Production":
            return {}

        return {
            "product_id": record["ID"],
        }

    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                if child_context.get("product_id"):
                    child_stream.sync(context=child_context)


class ProductProductionBOM(ChildStream):
    name = "product_production_bom"
    path = "/production/productionBOM"
    primary_keys = ["ProductID"]
    replication_key = None
    ignore_parent_replication_key = True
    parent_stream_type = ProductStream
    no_pagination_req = True
    multiple_request_context = [] 

    def get_url_params(self, context={}, next_page_token=""):
        params = super().get_url_params(context, next_page_token)
        params["ProductID"] = context["product_id"]
        return params

    schema = th.PropertiesList(
        th.Property("ProductID", th.StringType),
        th.Property("ProductionBoms", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class SaleListStream(hotglueStream):
    name = "sales_list"
    path = "/saleList"
    primary_keys = ["SaleID"]
    replication_key = "Updated"
    records_jsonpath = "$.SaleList[*]"

    schema = th.PropertiesList(
        th.Property("SaleID", th.StringType),
        th.Property("OrderNumber", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("OrderDate", th.StringType),
        th.Property("InvoiceDate", th.StringType),
        th.Property("Customer", th.StringType),
        th.Property("CustomerID", th.StringType),
        th.Property("InvoiceNumber", th.StringType),
        th.Property("CustomerReference", th.StringType),
        th.Property("InvoiceAmount", th.NumberType),
        th.Property("PaidAmount", th.NumberType),
        th.Property("InvoiceDueDate", th.StringType),
        th.Property("ShipBy", th.StringType),
        th.Property("BaseCurrency", th.StringType),
        th.Property("CustomerCurrency", th.StringType),
        th.Property("CreditNoteNumber", th.StringType),
        th.Property("Updated", th.DateTimeType),
        th.Property("QuoteStatus", th.StringType),
        th.Property("OrderStatus", th.StringType),
        th.Property("CombinedPickingStatus", th.StringType),
        th.Property("CombinedPaymentStatus", th.StringType),
        th.Property("CombinedTrackingNumbers", th.StringType),
        th.Property("CombinedPackingStatus", th.StringType),
        th.Property("CombinedShippingStatus", th.StringType),
        th.Property("CombinedInvoiceStatus", th.StringType),
        th.Property("CreditNoteStatus", th.StringType),
        th.Property("FulFilmentStatus", th.StringType),
        th.Property("Type", th.StringType),
        th.Property("SourceChannel", th.StringType),
        th.Property("ExternalID", th.StringType),
        th.Property("OrderLocationID", th.StringType),
        th.Property("RestockStatus", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "sale_id": record["SaleID"],
        }


class SaleDetailsStream(ChildStream):
    name = "sale_with_details"
    path = "/sale?ID={sale_id}"
    primary_keys = ["ID"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.[*]"
    parent_stream_type = SaleListStream
    no_pagination_req = True
    multiple_request_context = [] 

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("BaseCurrency", th.StringType),
        th.Property("CustomerCurrency", th.StringType),
        th.Property("SaleOrderDate", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
        th.Property("Location", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("CombinedPickingStatus", th.StringType),
        th.Property("CombinedPackingStatus", th.StringType),
        th.Property("CombinedShippingStatus", th.StringType),
        th.Property("FulFilmentStatus", th.StringType),
        th.Property("CombinedInvoiceStatus", th.StringType),
        th.Property("CombinedPaymentStatus", th.StringType),
        th.Property("BillingAddress", th.CustomType({"type": ["object", "string"]})),
        th.Property("ShippingAddress", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "Order",
            th.ObjectType(
                th.Property("SaleOrderNumber", th.StringType),
                th.Property("Memo", th.StringType),
                th.Property("Status", th.StringType),
                th.Property("TotalBeforeTax", th.NumberType),
                th.Property("Tax", th.NumberType),
                th.Property("Total", th.NumberType),
                th.Property(
                    "Lines",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("ProductID", th.StringType),
                            th.Property("SKU", th.StringType),
                            th.Property("Name", th.StringType),
                            th.Property("Quantity", th.NumberType),
                            th.Property("Price", th.NumberType),
                            th.Property("Discount", th.NumberType),
                            th.Property("Tax", th.NumberType),
                            th.Property("AverageCost", th.NumberType),
                            th.Property("TaxRule", th.StringType),
                            th.Property("Total", th.NumberType),
                        )
                    ),
                ),
            ),
        ),
        th.Property(
            "Invoices",
            th.ArrayType(th.ObjectType(th.Property("InvoiceDate", th.StringType))),
        ),
        th.Property(
            "Fulfilments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TaskID", th.StringType),
                    th.Property("FulfillmentNumber", th.IntegerType),
                    th.Property("LinkedInvoiceNumber", th.StringType),
                    th.Property("FulFilmentStatus", th.StringType),
                    th.Property("Pick", th.ObjectType(
                        th.Property("Status", th.StringType),
                        th.Property("Lines", th.ArrayType(th.ObjectType(
                            th.Property("ProductID", th.StringType),
                            th.Property("SKU", th.StringType),
                            th.Property("Name", th.StringType),
                            th.Property("Location", th.StringType),
                            th.Property("LocationID", th.StringType),
                            th.Property("Quantity", th.NumberType),
                            th.Property("BatchSN", th.StringType),
                            th.Property("ExpiryDate", th.DateTimeType),
                            th.Property("Box", th.StringType),
                            th.Property("NonInventory", th.BooleanType),
                            th.Property("WarrantyRegistrationNumber", th.StringType),
                            th.Property("RestockLocation", th.StringType),
                            th.Property("RestockLocationID", th.StringType),
                        ))),
                    )),
                    th.Property("Pack", th.ObjectType(
                        th.Property("Status", th.StringType),
                        th.Property("Lines", th.ArrayType(th.ObjectType(
                            th.Property("ProductID", th.StringType),
                            th.Property("SKU", th.StringType),
                            th.Property("Name", th.StringType),
                            th.Property("Location", th.StringType),
                            th.Property("LocationID", th.StringType),
                            th.Property("Quantity", th.NumberType),
                            th.Property("BatchSN", th.StringType),
                            th.Property("ExpiryDate", th.DateTimeType),
                            th.Property("Box", th.StringType),
                            th.Property("NonInventory", th.BooleanType),
                            th.Property("WarrantyRegistrationNumber", th.StringType),
                            th.Property("RestockLocation", th.StringType),
                            th.Property("RestockLocationID", th.StringType),
                        ))),
                    )),
                    th.Property("Ship", th.ObjectType(
                        th.Property("Status", th.StringType),
                        th.Property("RequireBy", th.DateType),
                        th.Property("ShippingAddress", th.ObjectType(
                            th.Property("ID", th.StringType),
                            th.Property("DisplayAddressLine1", th.StringType),
                            th.Property("DisplayAddressLine2", th.StringType),
                            th.Property("Line1", th.StringType),
                            th.Property("Line2", th.StringType),
                            th.Property("City", th.StringType),
                            th.Property("State", th.StringType),
                            th.Property("Postcode", th.StringType),
                            th.Property("Country", th.StringType),
                            th.Property("Company", th.StringType),
                            th.Property("Contact", th.StringType),
                            th.Property("ShipToOther", th.BooleanType),
                        )),
                        th.Property("ShippingNotes", th.StringType),
                        th.Property("Lines", th.ArrayType(th.ObjectType(
                            th.Property("ID", th.StringType),
                            th.Property("ShipmentDate", th.DateTimeType),
                            th.Property("Carrier", th.StringType),
                            th.Property("Boxes", th.StringType),
                            th.Property("TrackingNumber", th.StringType),
                            th.Property("TrackingURL", th.StringType),
                            th.Property("IsShipped", th.BooleanType),
                        ))),
                    )),
                )
            )
        )
    ).to_dict()


class SaleFulfilmentStream(ChildStream):
    name = "sale_fulfilment_list"
    path = "/sale/fulfilment/?SaleID={sale_id}&IncludeProductInfo=True"
    primary_keys = ["SaleID"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$[*]"
    parent_stream_type = SaleListStream
    multiple_request_context = [] 

    no_pagination_req = True
    schema = th.PropertiesList(
        th.Property("SaleID", th.StringType),
        th.Property(
            "Fulfilments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TaskID", th.StringType),
                    th.Property("FulfillmentNumber", th.IntegerType),
                    th.Property("LinkedInvoiceNumber", th.StringType),
                    th.Property("FulFilmentStatus", th.StringType),
                )
            ),
        ),
        th.Property(
            "Products",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ID", th.StringType),
                    th.Property("SKU", th.StringType),
                    th.Property("modifiedDate", th.StringType),
                    th.Property("Name", th.StringType),
                    th.Property("Category", th.StringType),
                    th.Property("Brand", th.StringType),
                    th.Property("Type", th.StringType),
                    th.Property("CostingMethod", th.StringType),
                    th.Property("DropShipMode", th.StringType),
                    th.Property("DefaultLocation", th.StringType),
                    th.Property("Length", th.NumberType),
                    th.Property("Width", th.NumberType),
                    th.Property("Height", th.NumberType),
                    th.Property("Weight", th.NumberType),
                    th.Property("UOM", th.StringType),
                    th.Property("WeightUnits", th.StringType),
                    th.Property("DimensionsUnits", th.StringType),
                    th.Property("Barcode", th.StringType),
                    th.Property("MinimumBeforeReorder", th.NumberType),
                    th.Property("ReorderQuantity", th.NumberType),
                    th.Property("PriceTier1", th.NumberType),
                    th.Property("PriceTier2", th.NumberType),
                    th.Property("PriceTier3", th.NumberType),
                    th.Property("PriceTier4", th.NumberType),
                    th.Property("PriceTier5", th.NumberType),
                    th.Property("PriceTier6", th.NumberType),
                    th.Property("PriceTier7", th.NumberType),
                    th.Property("PriceTier8", th.NumberType),
                    th.Property("PriceTier9", th.NumberType),
                    th.Property("PriceTier10", th.NumberType),
                    th.Property("AverageCost", th.NumberType),
                    th.Property("ShortDescription", th.StringType),
                    th.Property("InternalNote", th.StringType),
                    th.Property("AdditionalAttribute1", th.StringType),
                    th.Property("AdditionalAttribute2", th.StringType),
                    th.Property("AdditionalAttribute3", th.StringType),
                    th.Property("AdditionalAttribute4", th.StringType),
                    th.Property("AdditionalAttribute5", th.StringType),
                    th.Property("AdditionalAttribute6", th.StringType),
                    th.Property("AdditionalAttribute7", th.StringType),
                    th.Property("AdditionalAttribute8", th.StringType),
                    th.Property("AdditionalAttribute9", th.StringType),
                    th.Property("AdditionalAttribute10", th.StringType),
                    th.Property("AttributeSet", th.StringType),
                    th.Property("DiscountRule", th.StringType),
                    th.Property("Tags", th.StringType),
                    th.Property("Status", th.StringType),
                    th.Property("StockLocator", th.StringType),
                    th.Property("COGSAccount", th.StringType),
                    th.Property("RevenueAccount	", th.StringType),
                    th.Property("ExpenseAccount", th.StringType),
                    th.Property("InventoryAccount", th.StringType),
                    th.Property("PurchaseTaxRule", th.StringType),
                    th.Property("SaleTaxRule", th.StringType),
                    th.Property("LastModifiedOn", th.DateTimeType),
                    th.Property("Sellable", th.BooleanType),
                    th.Property("PickZones", th.StringType),
                    th.Property("BillOfMaterial", th.BooleanType),
                    th.Property("AutoAssembly", th.BooleanType),
                    th.Property("AutoDisassembly", th.BooleanType),
                    th.Property("QuantityToProduce", th.NumberType),
                    th.Property("AssemblyInstructionURL", th.StringType),
                    th.Property("AssemblyCostEstimationMethod", th.StringType),
                    th.Property("BOMType", th.StringType),
                    th.Property(
                        "Suppliers",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("SupplierID", th.StringType),
                                th.Property("SupplierName", th.StringType),
                                th.Property("SupplierInventoryCode", th.StringType),
                                th.Property("SupplierProductName", th.StringType),
                                th.Property("Cost", th.NumberType),
                                th.Property("FixedCost", th.NumberType),
                                th.Property("Currency", th.StringType),
                                th.Property("DropShip", th.BooleanType),
                                th.Property("URL", th.StringType),
                            )
                        ),
                    ),
                    th.Property(
                        "ReorderLevels",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("LocationID", th.StringType),
                                th.Property("LocationName", th.StringType),
                                th.Property("MinimumBeforeReorder", th.NumberType),
                                th.Property("ReorderQuantity", th.NumberType),
                                th.Property("StockLocator", th.StringType),
                                th.Property("PickZones", th.StringType),
                            )
                        ),
                    ),
                    th.Property(
                        "BillOfMaterialsProducts",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("ComponentProductID", th.StringType),
                                th.Property("ProductCode", th.StringType),
                                th.Property("Name", th.StringType),
                                th.Property("Quantity", th.NumberType),
                                th.Property("WastagePercent", th.NumberType),
                                th.Property("WastageQuantity", th.NumberType),
                                th.Property("CostPercentage", th.NumberType),
                            )
                        ),
                    ),
                    th.Property(
                        "BillOfMaterialsServices",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("ComponentProductID", th.StringType),
                                th.Property("ExpenseAccount", th.StringType),
                                th.Property("Name", th.StringType),
                                th.Property("Quantity", th.NumberType),
                                th.Property("PriceTier", th.IntegerType),
                                th.Property("ExpenseAccount", th.NumberType),
                            )
                        ),
                    ),
                    th.Property(
                        "Movements",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("TaskID", th.StringType),
                                th.Property("Type", th.StringType),
                                th.Property("Date", th.StringType),
                                th.Property("Number", th.StringType),
                                th.Property("Status", th.IntegerType),
                                th.Property("Quantity", th.NumberType),
                                th.Property("Amount", th.NumberType),
                                th.Property("Location", th.NumberType),
                                th.Property("BatchSN", th.NumberType),
                                th.Property("ExpiryDate", th.StringType),
                                th.Property("FromTo", th.StringType),
                            )
                        ),
                    ),
                    th.Property(
                        "Attachments",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("ID", th.StringType),
                                th.Property("ContentType", th.StringType),
                                th.Property("IsDefault", th.BooleanType),
                                th.Property("FileName", th.StringType),
                                th.Property("DownloadUrl", th.IntegerType),
                            )
                        ),
                    ),
                    th.Property(
                        "CustomPrices",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("ProductID", th.StringType),
                                th.Property("CustomerID", th.StringType),
                                th.Property("CustomerName", th.StringType),
                                th.Property("ProductSKU", th.StringType),
                                th.Property("ProductName", th.StringType),
                                th.Property("Price", th.NumberType),
                            )
                        ),
                    ),
                    th.Property("BOMType", th.StringType),
                    th.Property("WarrantyName", th.StringType),
                )
            ),
        ),
    ).to_dict()


class ProductAvalibilityStream(hotglueStream):
    name = "product_availability"
    path = "/ref/productavailability"
    primary_keys = ["ID"]
    replication_key = None
    records_jsonpath = "$.ProductAvailabilityList[*]"

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Barcode", th.StringType),
        th.Property("Location", th.StringType),
        th.Property("Bin", th.StringType),
        th.Property("Batch", th.StringType),
        th.Property("ExpiryDate", th.StringType),
        th.Property("OnHand", th.NumberType),
        th.Property("Allocated", th.NumberType),
        th.Property("Available", th.NumberType),
        th.Property("OnOrder", th.NumberType),
        th.Property("StockOnHand", th.NumberType),
    ).to_dict()


class SupplierStream(hotglueStream):
    name = "supplier_list"
    path = "/supplier"
    primary_keys = ["ID"]
    replication_key = "LastModifiedOn"
    records_jsonpath = "$.SupplierList[*]"

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Currency", th.StringType),
        th.Property("PaymentTerm", th.StringType),
        th.Property("TaxRule", th.StringType),
        th.Property("Discount", th.NumberType),
        th.Property("Comments", th.StringType),
        th.Property("AccountPayable", th.StringType),
        th.Property("TaxNumber", th.StringType),
        th.Property("AdditionalAttribute1", th.StringType),
        th.Property("AdditionalAttribute2", th.StringType),
        th.Property("AdditionalAttribute3", th.StringType),
        th.Property("AdditionalAttribute4", th.StringType),
        th.Property("AdditionalAttribute5", th.StringType),
        th.Property("AdditionalAttribute6", th.StringType),
        th.Property("AdditionalAttribute7", th.StringType),
        th.Property("AdditionalAttribute8", th.StringType),
        th.Property("AdditionalAttribute9", th.StringType),
        th.Property("AdditionalAttribute10", th.StringType),
        th.Property("AttributeSet", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("LastModifiedOn", th.DateTimeType),
        th.Property(
            "Addresses",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ID", th.StringType),
                    th.Property("Line1", th.StringType),
                    th.Property("Line2", th.StringType),
                    th.Property("City", th.StringType),
                    th.Property("State", th.StringType),
                    th.Property("Postcode", th.StringType),
                    th.Property("Country", th.StringType),
                    th.Property("Type", th.StringType),
                    th.Property("DefaultForType", th.BooleanType),
                )
            ),
        ),
        th.Property(
            "Contacts",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ID", th.StringType),
                    th.Property("Name", th.StringType),
                    th.Property("Phone", th.StringType),
                    th.Property("MobilePhone", th.StringType),
                    th.Property("Fax", th.StringType),
                    th.Property("Email", th.StringType),
                    th.Property("Website", th.StringType),
                    th.Property("Comment", th.StringType),
                    th.Property("Default", th.BooleanType),
                    th.Property("IncludeInEmail", th.BooleanType),
                )
            ),
        ),
    ).to_dict()


class PurchaseStream(hotglueStream):
    name = "purchase_list"
    path = "/purchaseList"
    primary_keys = ["ID"]
    replication_key = "LastUpdatedDate"
    records_jsonpath = "$.PurchaseList[*]"

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("BlindReceipt", th.BooleanType),
        th.Property("OrderNumber", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("OrderDate", th.StringType),
        th.Property("InvoiceDate", th.StringType),
        th.Property("Supplier", th.StringType),
        th.Property("SupplierID", th.StringType),
        th.Property("InvoiceNumber", th.StringType),
        th.Property("InvoiceAmount", th.NumberType),
        th.Property("PaidAmount", th.NumberType),
        th.Property("InvoiceDueDate", th.StringType),
        th.Property("RequiredBy", th.StringType),
        th.Property("BaseCurrency", th.StringType),
        th.Property("SupplierCurrency", th.StringType),
        th.Property("CreditNoteNumber", th.StringType),
        th.Property("OrderStatus", th.StringType),
        th.Property("StockReceivedStatus", th.StringType),
        th.Property("UnstockStatus", th.StringType),
        th.Property("InvoiceStatus", th.StringType),
        th.Property("CreditNoteStatus", th.StringType),
        th.Property("LastUpdatedDate", th.DateTimeType),
        th.Property("Type", th.StringType),
        th.Property("CombinedInvoiceStatus", th.StringType),
        th.Property("CombinedPaymentStatus", th.StringType),
        th.Property("CombinedReceivingStatus", th.StringType),
        th.Property("IsServiceOnly", th.BooleanType),
        th.Property("DropShipTaskID", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "advance_sale_id": record["ID"],
        }


class AdvancePurchaseStream(ChildStream):
    name = "advance_purchase"
    path = "/advanced-purchase?ID={advance_sale_id}"
    primary_keys = ["ID"]
    replication_key = "LastUpdatedDate"
    records_jsonpath = "$.[*]"
    no_pagination_req = True
    parent_stream_type = PurchaseStream
    multiple_request_context = [] 

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("SupplierID", th.StringType),
        th.Property("Supplier", th.StringType),
        th.Property("BaseCurrency", th.StringType),
        th.Property("SupplierCurrency", th.StringType),
        th.Property("Location", th.StringType),
        th.Property("OrderNumber", th.StringType),
        th.Property("CombinedInvoiceStatus", th.StringType),
        th.Property("CombinedReceivingStatus", th.StringType),
        th.Property("CombinedPaymentStatus", th.StringType),
        th.Property("OrderDate", th.StringType),
        th.Property("Status", th.StringType),
        th.Property("LastUpdatedDate", th.DateTimeType),
        th.Property("RequiredBy", th.DateTimeType),
        th.Property(
            "Order",
            th.ObjectType(
                th.Property("Status", th.StringType),
                th.Property(
                    "Lines",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("ProductID", th.StringType),
                            th.Property("SKU", th.StringType),
                            th.Property("Name", th.StringType),
                            th.Property("Quantity", th.NumberType),
                            th.Property("Price", th.NumberType),
                            th.Property("Discount", th.NumberType),
                            th.Property("SupplierSKU", th.StringType),
                            th.Property("Total", th.NumberType),
                        )
                    ),
                ),
            ),
        ),
        th.Property(
            "Invoice",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TaskID", th.StringType),
                    th.Property("InvoicingAndReceivingNumber", th.IntegerType),
                    th.Property("InvoiceDate", th.StringType),
                    th.Property("InvoiceDueDate", th.StringType),
                    th.Property("InvoiceNumber", th.StringType),
                    th.Property("Status", th.StringType),
                )
            ),
        ),
        th.Property("Terms", th.StringType),
        th.Property("Type", th.StringType),
        th.Property("CurrencyRate", th.NumberType),
        th.Property(
            "StockReceived",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TaskID", th.StringType),
                    th.Property("InvoicingAndReceivingNumber", th.IntegerType),
                    th.Property("Status", th.StringType),
                    th.Property("lines", th.CustomType({"type": ["array", "string"]})),
                )
            ),
        ),
        th.Property(
            "PutAway",
            th.ArrayType(
                th.ObjectType(
                    th.Property("TaskID", th.StringType),
                    th.Property("InvoicingAndReceivingNumber", th.IntegerType),
                    th.Property("Status", th.StringType),
                    th.Property("lines", th.CustomType({"type": ["array", "string"]})),
                )
            ),
        ),
    ).to_dict()


class LocationStream(hotglueStream):
    name = "location"
    path = "/ref/location"
    primary_keys = ["ID"]
    records_jsonpath = "$.LocationList[*]"

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("IsDefault", th.BooleanType),
        th.Property("IsDeprecated", th.BooleanType),
        th.Property(
            "Bins",
            th.ArrayType(
                th.ObjectType(
                    th.Property("ID", th.StringType),
                    th.Property("Name", th.StringType),
                    th.Property("IsDeprecated", th.BooleanType),
                    th.Property("IsStaging", th.BooleanType),
                )
            ),
        ),
        th.Property("FixedAssetsLocation", th.BooleanType),
        th.Property("ParentID", th.StringType),
        th.Property("ParentName", th.StringType),
        th.Property("ReferenceCount", th.IntegerType),
        th.Property("AddressLine1", th.StringType),
        th.Property("AddressLine2", th.StringType),
        th.Property("AddressCitySuburb", th.StringType),
        th.Property("AddressStateProvince", th.StringType),
        th.Property("AddressZipPostCode", th.StringType),
        th.Property("AddressCountry", th.StringType),
        th.Property("PickZones", th.StringType),
        th.Property("IsShopfloor", th.BooleanType),
        th.Property("IsCoMan", th.BooleanType),
        th.Property("IsStaging", th.BooleanType),
    ).to_dict()


class MeStream(hotglueStream):
    name = "me"
    path = "/me"
    records_jsonpath = "$.[*]"
    no_pagination_req = True

    schema = th.PropertiesList(
        th.Property("Company", th.StringType),
        th.Property("Currency", th.StringType),
        th.Property("TimeZone", th.StringType),
        th.Property("DefaultWeightUnits", th.StringType),
        th.Property("DefaultDimensionsUnits", th.StringType),
        th.Property("LockDate", th.DateTimeType),
        th.Property("OpeningBalanceDate", th.DateTimeType),
        th.Property("TaxCalculationMethod", th.StringType),
        th.Property("DefaultSaleTaxRuleId", th.StringType),
        th.Property("DefaultSaleTaxRuleName", th.StringType),
        th.Property("MaximumDecimalPlacesInQuantity", th.StringType),
        th.Property("ApplyCustomerDiscountsAfterOtherDiscounts", th.BooleanType),
        th.Property("DiscountRule", th.StringType),
        th.Property("AutomaticallyApplyDiscounts", th.BooleanType),
        th.Property(
            "RoundingTable",
            th.ArrayType(
                th.ObjectType(
                    th.Property("RangeTo", th.NumberType),
                    th.Property("RoundToNearest", th.NumberType),
                    th.Property("AdjustmentRule", th.StringType),
                    th.Property("AdjustmentValue", th.NumberType),
                )
            ),
        ),
    ).to_dict()


class ProductionOrderListStream(hotglueStream):
    name = "production_order_list"
    path = "/production/orderList"
    records_jsonpath = "$.ProductionOrderListItems[*]"
    no_pagination_req = False

    schema = th.PropertiesList(
        th.Property("TaskID", th.StringType()),
        th.Property("ProductionOrderID", th.StringType()),
        th.Property("Type", th.StringType()),
        th.Property("ProductID", th.StringType()),
        th.Property("ProductSKU", th.StringType()),
        th.Property("ProductName", th.StringType()),
        th.Property("OrderNumber", th.StringType()),
        th.Property("LocationID", th.StringType()),
        th.Property("LocationName", th.StringType()),
        th.Property("Status", th.StringType()),
        th.Property("OrderStatus", th.StringType()),
        th.Property("Quantity", th.NumberType()),
        th.Property("StartDate", th.DateTimeType()),
        th.Property("ReleaseDate", th.DateTimeType()),
        th.Property("RequiredByDate", th.DateTimeType()),
        th.Property("CompletionDate", th.StringType()),
        th.Property("TotalCount", th.NumberType()),
        th.Property("CapacityCalculationType", th.StringType()),
        th.Property("WIPAccountCode", th.StringType()),
        th.Property("FinishedGoodsAccountCode", th.StringType()),
        th.Property("Comments", th.StringType()),
        th.Property("SourceTaskID", th.StringType()),
        th.Property("SourceTaskNumber", th.StringType()),
        th.Property("SourceTaskType", th.StringType()),
        th.Property("IsSourceTaskVoided", th.BooleanType()),
        th.Property("CustomField1", th.StringType()),
        th.Property("CustomField2", th.StringType()),
        th.Property("CustomField3", th.StringType()),
        th.Property("CustomField4", th.StringType()),
        th.Property("CustomField5", th.StringType()),
        th.Property("CustomField6", th.StringType()),
        th.Property("CustomField7", th.StringType()),
        th.Property("CustomField8", th.StringType()),
        th.Property("CustomField9", th.StringType()),
        th.Property("CustomField10", th.StringType()),
        th.Property(
            "SourceTasks",
            th.ArrayType(
                th.ObjectType(
                    th.Property("SourceTaskID", th.StringType()),
                    th.Property("SourceTaskNumber", th.StringType()),
                    th.Property("SourceTaskType", th.IntegerType()),
                    th.Property("IsSourceTaskVoided", th.BooleanType()),
                )
            ),
        ),
    ).to_dict()

    def get_child_context(self, record, context):
        return {
            "production_order_id": record["ProductionOrderID"],
        }


class ProductionOrderDetailStream(ChildStream):
    name = "production_order_detail"
    path = "/production/order?ProductionOrderID={production_order_id}"
    records_jsonpath = "$.ProductionOrders[*]"
    no_pagination_req = False
    parent_stream_type = ProductionOrderListStream
    multiple_request_context = [] 

    schema = th.PropertiesList(
        th.Property("ProductionOrderID", th.StringType()),
        th.Property("ProductID", th.StringType()),
        th.Property("ProductName", th.StringType()),
        th.Property("ProductSKU", th.StringType()),
        th.Property("OrderNumber", th.StringType()),
        th.Property("LocationID", th.StringType()),
        th.Property("LocationName", th.StringType()),
        th.Property("CostingMethod", th.StringType()),
        th.Property("WarehouseID", th.StringType()),
        th.Property("WarehouseName", th.StringType()),
        th.Property("Unit", th.StringType()),
        th.Property("OrderStatus", th.StringType()),
        th.Property("Status", th.StringType()),
        th.Property("SourceName", th.IntegerType()),
        th.Property("SourceTaskID", th.StringType()),
        th.Property("SourceTaskNumber", th.StringType()),
        th.Property("WIPAccountCode", th.StringType()),
        th.Property("FinishedGoodsAccountCode", th.StringType()),
        th.Property("Quantity", th.IntegerType()),
        th.Property("BOMQuantity", th.IntegerType()),
        th.Property("CapacityCalculationType", th.StringType()),
        th.Property("StartDate", th.DateTimeType()),
        th.Property("ReleaseDate", th.DateTimeType()),
        th.Property("RequiredByDate", th.DateTimeType()),
        th.Property("CompletionDate", th.StringType()),
        th.Property("IsIgnoreLeadTime", th.BooleanType()),
        th.Property("RetailID", th.StringType()),
        th.Property("Comments", th.StringType()),
        th.Property("StartUpdate", th.BooleanType()),
        th.Property("PlannedBy", th.StringType()),
        th.Property("ReleasedBy", th.StringType()),
        th.Property("OrderCycleTime", th.IntegerType()),
        th.Property("BOMVersion", th.IntegerType()),
        th.Property("BOMName", th.StringType()),
        th.Property("Tags", th.StringType()),
        th.Property("CustomField1", th.StringType()),
        th.Property("CustomField2", th.StringType()),
        th.Property("CustomField3", th.StringType()),
        th.Property("CustomField4", th.StringType()),
        th.Property("CustomField5", th.StringType()),
        th.Property("CustomField6", th.StringType()),
        th.Property("CustomField7", th.StringType()),
        th.Property("CustomField8", th.StringType()),
        th.Property("CustomField9", th.StringType()),
        th.Property("CustomField10", th.StringType()),
        th.Property("IssueMethodComponent", th.IntegerType()),
        th.Property("IssueMethodParameter", th.IntegerType()),
        th.Property("RunSize", th.IntegerType()),
        th.Property(
            "Operations",
            th.ArrayType(
                th.ObjectType(
                    th.Property("OperationID", th.StringType()),
                    th.Property("Order", th.IntegerType()),
                    th.Property("Name", th.StringType()),
                    th.Property("CycleTime", th.IntegerType()),
                    th.Property("UnitsPerCycle", th.IntegerType()),
                    th.Property("TotalCycleTime", th.IntegerType()),
                    th.Property("TotalUnitsPerCycle", th.IntegerType()),
                    th.Property("OperationType", th.StringType()),
                    th.Property("WorkCenterID", th.StringType()),
                    th.Property("WorkCenterName", th.StringType()),
                    th.Property("WorkCenterCode", th.StringType()),
                    th.Property("WorkCenterCoManProcurementType", th.StringType()),
                    th.Property("ComponentLocationID", th.StringType()),
                    th.Property("IsDropShip", th.BooleanType()),
                    th.Property("IsBackflush", th.BooleanType()),
                    th.Property(
                        "Attachments",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("AttachmentID", th.StringType()),
                                th.Property("FileName", th.StringType()),
                                th.Property("Date", th.DateTimeType()),
                                th.Property("ContentType", th.StringType()),
                                th.Property("Position", th.IntegerType()),
                                th.Property("Content", th.StringType()),
                            )
                        ),
                    ),
                    th.Property(
                        "Components",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("OrderComponentID", th.StringType()),
                                th.Property("ProductID", th.StringType()),
                                th.Property("ProductName", th.StringType()),
                                th.Property("ProductSKU", th.StringType()),
                                th.Property("Available", th.IntegerType()),
                                th.Property("Quantity", th.IntegerType()),
                                th.Property("TotalQuantity", th.IntegerType()),
                                th.Property("WastageQty", th.IntegerType()),
                                th.Property("WastagePercent", th.IntegerType()),
                                th.Property("Cost", th.NumberType()),
                                th.Property("TotalCost", th.NumberType()),
                                th.Property("ProductCost", th.NumberType()),
                                th.Property("CostingMethod", th.StringType()),
                                th.Property("Unit", th.StringType()),
                                th.Property("ProductType", th.StringType()),
                                th.Property("Position", th.IntegerType()),
                                th.Property("SalePriceTier", th.IntegerType()),
                                th.Property("IsAlternative", th.BooleanType()),
                                th.Property("IsBackflush", th.BooleanType()),
                            )
                        ),
                    ),
                    th.Property(
                        "Resources",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("OrderResourceID", th.StringType()),
                                th.Property("ResourceID", th.StringType()),
                                th.Property("ResourceCode", th.StringType()),
                                th.Property("ResourceName", th.StringType()),
                                th.Property("Quantity", th.IntegerType()),
                                th.Property("Cost", th.IntegerType()),
                                th.Property("TotalCost", th.IntegerType()),
                                th.Property("ResourceCost", th.IntegerType()),
                                th.Property("CycleTime", th.IntegerType()),
                                th.Property("Position", th.IntegerType()),
                                th.Property("CostCalculationType", th.StringType()),
                            )
                        ),
                    ),
                    th.Property(
                        "OperationLinks",
                        th.ArrayType(
                            th.ObjectType(
                                th.Property("ID", th.StringType()),
                                th.Property("RelatedOperationID", th.StringType()),
                                th.Property("RelationType", th.IntegerType()),
                            )
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "SourceTasks",
            th.ArrayType(
                th.ObjectType(
                    th.Property("SourceTaskID", th.StringType()),
                    th.Property("SourceTaskNumber", th.StringType()),
                )
            ),
        ),
    ).to_dict()

class TaxRulesStream(hotglueStream):
    name = "tax_list"
    path = "/ref/tax"
    primary_keys = ["ID"]
    records_jsonpath = "$.TaxRuleList[*]"

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("Account", th.StringType),
        th.Property("IsActive", th.BooleanType),
        th.Property("TaxInclusive", th.BooleanType),
        th.Property("TaxPercent", th.NumberType),
        th.Property("IsTaxForSale", th.BooleanType),
        th.Property("IsTaxForPurchase", th.BooleanType),
        th.Property("Components", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()
class UnitListStream(hotglueStream):
    name = "unit_list"
    path = "/ref/unit"
    primary_keys = ["ID"]
    records_jsonpath = "$.UnitList[*]"

    schema = th.PropertiesList(
        th.Property("ID", th.StringType),
        th.Property("Name", th.StringType),
    ).to_dict()

class StockTransferListStream(hotglueStream):
    name = "stock_transfer_list"
    path = "/stockTransferList"
    primary_keys = ["TaskID"]
    records_jsonpath = "$.StockTransferList[*]"
    no_pagination_req = False
    
    schema = th.PropertiesList(
        th.Property("TaskID", th.StringType()),
        th.Property("From", th.StringType()),
        th.Property("FromLocation", th.StringType()),
        th.Property("To", th.StringType()),
        th.Property("ToLocation", th.StringType()),
        th.Property("Status", th.StringType()),
        th.Property("Number", th.StringType()),
        th.Property("CompletionDate", th.DateTimeType()),
        th.Property("DepartureDate", th.DateTimeType()),
        th.Property("InTransitAccount", th.StringType()),
        th.Property("CostDistributionType", th.StringType()),
        th.Property("Reference", th.StringType()),
        th.Property("SkipOrder", th.BooleanType()),
    ).to_dict()

    def get_child_context(self, record, context):
        return {
            "task_id": record["TaskID"],
        }
    
    def _sync_children(self, child_context: dict) -> None:
        for child_stream in self.child_streams:
            if child_stream.selected or child_stream.has_selected_descendents:
                if child_context.get("task_id"):
                    child_stream.sync(context=child_context)
                    
class StockTransferStream(ChildStream):
    name = "stock_transfer"
    path = "/stockTransfer?TaskID={task_id}"
    primary_keys = ["ProductID"]
    records_jsonpath = "$.[*]"
    no_pagination_req = True
    parent_stream_type = StockTransferListStream
    multiple_request_context = []
    
    stock_transfer_order_line = [
        th.Property("ProductID", th.StringType()),
        th.Property("SKU", th.StringType()),
        th.Property("ProductName", th.StringType()),
        th.Property("QuantityOnHand", th.NumberType()),
        th.Property("QuantityAvailable", th.NumberType()),
        th.Property("TransferQuantity", th.NumberType()),
        th.Property("BatchSN", th.StringType()),
        th.Property("ExpiryDate", th.DateTimeType()),
        th.Property("Comments", th.StringType()),
        th.Property("ProductLength", th.NumberType()),
        th.Property("ProductWidth", th.NumberType()),
        th.Property("ProductHeight", th.NumberType()),
        th.Property("ProductWeight", th.NumberType()),
        th.Property("WeightUnits", th.StringType()),
        th.Property("DimensionsUnits", th.StringType()),
        *[th.Property(f"ProductCustomField{i+1}", th.StringType()) for i in range(10)],
        *[th.Property(f"PriceTear{i+1}", th.NumberType()) for i in range(10)],
        th.Property("Barcode", th.StringType()),
    ]

    schema = th.PropertiesList(
        th.Property("TaskID", th.StringType()),
        th.Property("From", th.StringType()),
        th.Property("FromLocation", th.StringType()),
        th.Property("To", th.StringType()),
        th.Property("ToLocation", th.StringType()),
        th.Property("Status", th.StringType()),
        th.Property("Number", th.StringType()),
        th.Property("CostDistributionType", th.StringType()),
        th.Property("InTransitAccount", th.StringType()),
        th.Property("DepartureDate", th.DateTimeType()),
        th.Property("CompletionDate", th.DateTimeType()),
        th.Property("RequiredByDate", th.DateTimeType()),
        th.Property("Reference", th.StringType()),
        th.Property("SkipOrder", th.BooleanType()),
        th.Property(
            "Lines",
            th.ArrayType(
                th.ObjectType(
                    *stock_transfer_order_line
                )
            )
        ),
        th.Property(
            "Order",
            th.ObjectType(
                th.Property("Status", th.StringType()),
                th.Property("Lines",
                    th.ArrayType(
                        th.ObjectType(
                            *stock_transfer_order_line
                        )
                    )
                )
            )
        ),
    ).to_dict()