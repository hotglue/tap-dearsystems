"""REST client handling, including hotglueStream base class."""

import time
from typing import Optional, Any, Generator, Dict, Callable, List, cast, Iterable
import backoff
import requests
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream
import concurrent.futures
import copy
from singer_sdk.exceptions import InvalidStreamSortException
from singer_sdk.helpers._state import (
    finalize_state_progress_markers,
    get_state_partitions_list,
    log_sort_error,
)


def _find_in_partitions_list(
    partitions: List[dict], state_partition_context: dict
) -> Optional[dict]:
    found = [
        partition_state
        for partition_state in partitions
        if partition_state["context"] == state_partition_context
    ]
    if len(found) > 1:
        raise ValueError(
            f"State file contains duplicate entries for partition: "
            "{state_partition_context}.\n"
            f"Matching state values were: {str(found)}"
        )
    if found:
        return cast(dict, found[0])

    return None


def get_state_if_exists(
    tap_state: dict,
    tap_stream_id: str,
    state_partition_context: Optional[dict] = None,
    key: Optional[str] = None,
) -> Optional[Any]:
    if "bookmarks" not in tap_state:
        return None
    if tap_stream_id not in tap_state["bookmarks"]:
        return None

    skip_incremental_partitions = [
        "product_production_bom",
        "sale_with_details",
        "sale_fulfilment_list",
        "advance_purchase",
        "production_order_detail",
        
    ]
    stream_state = tap_state["bookmarks"][tap_stream_id]
    if tap_stream_id in skip_incremental_partitions and "partitions" in stream_state:
        # stream_state["partitions"] = []
        partitions = stream_state["partitions"][len(stream_state["partitions"]) - 1][
            "context"
        ]
        stream_state["partitions"] = [{"context": partitions}]

    if not state_partition_context:
        if key:
            return stream_state.get(key, None)
        return stream_state
    if "partitions" not in stream_state:
        return None  # No partitions defined

    matched_partition = _find_in_partitions_list(
        stream_state["partitions"], state_partition_context
    )
    if matched_partition is None:
        return None  # Partition definition not present
    if key:
        return matched_partition.get(key, None)
    return matched_partition


def get_state_partitions_list(
    tap_state: dict, tap_stream_id: str
) -> Optional[List[dict]]:
    """Return a list of partitions defined in the state, or None if not defined."""
    return (get_state_if_exists(tap_state, tap_stream_id) or {}).get("partitions", None)
class hotglueStream(RESTStream):
    """hotglue stream class."""

    url_base = "https://inventory.dearsystems.com/ExternalApi/v2"
    records_jsonpath = "$[*]"
    next_page_token_jsonpath = "$.next_page"
    no_pagination_req = False
    additional_params = {}

    @property
    def partitions(self) -> Optional[List[dict]]:
        result: List[dict] = []
        for partition_state in (
            get_state_partitions_list(self.tap_state, self.name) or []
        ):
            result.append(partition_state["context"])
        if result is not None and len(result) > 0:
            result = [result[len(result) - 1]]
        return result or None

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["api-auth-accountid"] = self.config.get("api_account_id")
        headers["api-auth-applicationkey"] = self.config.get("api_key")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        if self.no_pagination_req:
            return None

        if previous_token is None:
            return 2

        len_of_list = len(
            list(extract_jsonpath(self.records_jsonpath, input=response.json()))
        )
        if len_of_list == 0:
            return None

        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["limit"] = 1000 # max of records per page
        if self.additional_params:
            params.update(self.additional_params)

        date_filters = {
            "LastModifiedOn": "ModifiedSince",
            "Updated": "UpdatedSince",
            "LastUpdatedDate": "UpdatedSince",
        }

        if self.replication_key:
            params[
                date_filters[self.replication_key]
            ] = self.get_starting_replication_key_value(context)

        if next_page_token is None:
            params["page"] = 1
        else:
            params["page"] = next_page_token
        return params

    def post_process(self, row: dict, context: Optional[dict]) -> dict:
        """As needed, append or transform raw data to match expected structure."""
        if "fulfilment" in self.get_url({}) and "Exception" in row.keys():
            return {"SaleID": "", "Fulfilments": [], "Products": []}
        if "sale_order" == self.name and "Exception" in row.keys():
            return {
                "SaleID": "",
                "SaleOrderNumber": "",
                "Memo": "",
                "Status": "",
                "Lines": [],
                "AdditionalCharges": [],
                "TotalBeforeTax": 0,
                "Tax": 0,
                "Total": 0,
            }
        return row

    def validate_response(self, response: requests.Response) -> None:
        """Validate HTTP response."""
        if response.status_code == 503:
            msg = (
                f"{response.status_code} Reach maximum hit on api: "
                f"{response.reason} for path: {self.path}"
            )
            time.sleep(30)
            raise RetriableAPIError(msg)

        if 400 < response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)

        elif 500 <= response.status_code < 600:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)

    def request_decorator(self, func: Callable) -> Callable:
        """Instantiate a decorator for handling request failures."""

        decorator: Callable = backoff.on_exception(
            backoff.expo,
            (
                RetriableAPIError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.RequestException,
                requests.exceptions.Timeout,
                requests.exceptions.ConnectionError,
            ),
            max_tries=10,
            factor=2,
        )(func)
        return decorator

    def backoff_wait_generator() -> Callable[..., Generator[int, Any, None]]:
        return backoff.constant(interval=60)
    
    def _sync_records(
        self, context: Optional[dict] = None
    ) -> None:
        record_count = 0
        current_context: Optional[dict]
        context_list: Optional[List[dict]]
        context_list = [context] if context is not None else self.partitions
        selected = self.selected

        for current_context in context_list or [{}]:
            partition_record_count = 0
            current_context = current_context or None
            state = self.get_context_state(current_context)
            state_partition_context = self._get_state_partition_context(current_context)
            self._write_starting_replication_value(current_context)
            child_context: Optional[dict] = (
                None if current_context is None else copy.copy(current_context)
            )

            records = list(self.get_records(current_context))
            for i, record_result in enumerate(records):                
                if isinstance(record_result, tuple):
                    # Tuple items should be the record and the child context
                    record, child_context = record_result
                else:
                    record = record_result
                child_context = copy.copy(
                    self.get_child_context(record=record, context=child_context)
                )
                # CHANGE MADE: add value to the context indicating is the last record -> used for concurrency in child streams
                if child_context and i == (len(records) - 1) and self.child_streams:
                    child_context.update({"last_record": True})
                for key, val in (state_partition_context or {}).items():
                    # Add state context to records if not already present
                    if key not in record:
                        record[key] = val

                # Sync children, except when primary mapper filters out the record
                if self.stream_maps[0].get_filter_result(record):
                    self._sync_children(child_context)
                self._check_max_record_limit(record_count)
                if selected:
                    if (record_count - 1) % self.STATE_MSG_FREQUENCY == 0:
                        self._write_state_message()
                    self._write_record_message(record)
                    try:
                        self._increment_stream_state(record, context=current_context)
                    except InvalidStreamSortException as ex:
                        log_sort_error(
                            log_fn=self.logger.error,
                            ex=ex,
                            record_count=record_count + 1,
                            partition_record_count=partition_record_count + 1,
                            current_context=current_context,
                            state_partition_context=state_partition_context,
                            stream_name=self.name,
                        )
                        raise ex

                record_count += 1
                partition_record_count += 1
            if current_context == state_partition_context:
                # Finalize per-partition state only if 1:1 with context
                finalize_state_progress_markers(state)
        if not context:
            # Finalize total stream only if we have the full full context.
            # Otherwise will be finalized by tap at end of sync.
            finalize_state_progress_markers(self.stream_state)
        self._write_record_count_log(record_count=record_count, context=context)
        # Reset interim bookmarks before emitting final STATE message:
        self._write_state_message()


class ChildStream(hotglueStream):
    max_request_number = 10

    def request(self, context: Optional[dict]) -> Iterable[dict]:
        next_page_token: Any = None
        finished = False
        decorated_request = self.request_decorator(self._request)

        while not finished:
            if self.no_pagination_req:
                next_page_token = None
                
            prepared_request = self.prepare_request(
                context, next_page_token=next_page_token
            )
            resp = decorated_request(prepared_request, context)
            yield from self.parse_response(resp)

            if not self.no_pagination_req:
                previous_token = copy.deepcopy(next_page_token)
                next_page_token = self.get_next_page_token(
                    response=resp, previous_token=previous_token
                )
                if next_page_token and next_page_token == previous_token:
                    raise RuntimeError(
                        f"Loop detected in pagination. "
                        f"Pagination token {next_page_token} is identical to prior token."
                    )
            # Cycle until get_next_page_token() no longer returns a value
            finished = not next_page_token

    def concurrent_get(self, context):
        self.logger.info(f"Concurrent request for context {context}")
        with concurrent.futures.ThreadPoolExecutor(
                max_workers=self.max_request_number
            ) as executor:
            futures = [executor.submit(self.request, ctx) for ctx in context]
            results = []
            for future in concurrent.futures.as_completed(futures):
                result = future.result()
                for item in result:
                    results.append(item)
        return results

    def request_records(self, context: Optional[dict]) -> Iterable[dict]:
        if len(self.multiple_request_context) < self.max_request_number:
            self.multiple_request_context.append(context)
        if len(self.multiple_request_context) >= self.max_request_number or context.get("last_record"):
            for record in self.concurrent_get(self.multiple_request_context):
                yield record
            self.multiple_request_context = []